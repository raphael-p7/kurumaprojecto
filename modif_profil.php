<?php
$title = 'Profil';
require_once './include/header.php';

if (!isset($_SESSION['ID_UTI']) || empty($_SESSION['VALID_UTI'])) {
    header('Location: index.php');
}

if (!empty($_POST)){

try{
  $try = $pdo->prepare('SELECT MAIL_UTI FROM t_utilisateur WHERE MAIL_UTI = :mail');
  $try->execute([
      'mail' => $_POST['email']
  ]);
  $res = $try->fetch(PDO::FETCH_ASSOC);
}catch(PDOException $e){
  echo 'Échec lors de la connexion : ' . $e->getMessage();
}

  if($res['MAIL_UTI'] === $_POST['email']){
    echo "Erreur, l'adresse mail est déjà entrée dans la BDD";
  }else{

  $age = strlen($_POST['age']);

  $prenom = explode(" ", $_POST['prenom']);
  $prenom = implode("", $prenom);

  $marque = explode(" ", $_POST['marque']);
  $marque = implode("", $marque);

  $model = explode(" ", $_POST['model']);
  $model = implode("", $model);

  $mdp1 = $_POST['motdepasse'];
  $mdp2 = $_POST['confirmermotdepasse'];

  $place = strlen($_POST['place']);

  if (!ctype_alpha($_POST['prenom'])) {
      echo "Erreur, un prénom ne peut avoir de chiffre";
  } else {

    if ($age > 4) {
    echo "Erreur, age de mauvaise taille";
    } else {

      if ($place > 2) {
      echo "Erreur, le nombre de place de votre vehicule est trop grand";
      } else {

        if((empty($marque) || empty($model) || empty($_POST['place'])) && ($_POST['role'] === '1')){
          echo "Erreur, les details du vehicule sont incomplets";
        }else{

        if(empty($_POST['assurance']) && ($_POST['role'] === '1')){
          echo "Erreur, l'assurance est OBLIGATOIRE !!!'";
        }else{



          if( $_POST['role'] === '0'){
            $valide = '1';
          }else{
            $valide = '0';
          }

        if ($mdp1 == $mdp2) {

          if(empty($_POST['place'])){
            $_POST['place'] = '0';
          }

    $req = $pdo->prepare('UPDATE t_utilisateur SET MAIL_UTI = :email,
                                                  NOM_UTI = :prenom,
                                                  AGE_UTI = :age,
                                                  MDP_UTI = :motdepasse,
                                                  ROL_UTI = :role,
                                                  ADM_UTI = :admin,
                                                  MRQ_VOIT_UTI = :marque,
                                                  MDL_VOIT_UTI = :model,
                                                  NB_PL_VOIT_UTI = :place,
                                                  ASSU_VOIT_UTI = :assurance,
                                                  VALID_UTI = :valide
                                                  WHERE ID_UTI = '.$_SESSION['ID_UTI']);
    $req->execute([
        'prenom' => $prenom,
        'email' => $_POST['email'],
        'age' => $_POST['age'],
        'motdepasse' => password_hash($_POST['motdepasse'], PASSWORD_DEFAULT),
        'role' => $_POST['role'],
        'marque' => $marque,
        'model' => $model,
        'place' => $_POST['place'],
        'assurance' => $_POST['assurance'],
        'valide' => $valide,
        'admin' => '0',
    ]);

    $arrayHobbie = $_POST['hobbie'];

    foreach($_POST['hobbie'] as $valeur)
{
   $req2 = $pdo->prepare("UPDATE aimer SET ID_UTI = '".$_SESSION['ID_UTI']."' ,
                                          ID_HOB = :id_hobbies");
   $req2->execute([
     'id_hobbies' => $valeur,
   ]);
};

    header('Location: connection.php');
  }else{
    echo "mot de passe non conforme";
  }
}
}
}
}
}
}
}else{
  $result=$pdo->query('SELECT * FROM t_utilisateur WHERE ID_UTI = '.$_SESSION['ID_UTI']);
 $profil = $result->fetch(PDO::FETCH_ASSOC);
 $_SESSION['ID_UTI'] = $profil['ID_UTI'];
 $_SESSION['VALID_UTI'] = $profil['VALID_UTI'];
}

?>
<form action="inscription.php" method="POST">
    <label for="prenom">Prenom : </label> <input type="text" name="prenom" id="prenom" value="<?= $profil['NOM_UTI'] ?>"><br>
    <label for="age">Age : </label> <input type="number" name="age" id="age" value="<?= $profil['AGE_UTI'] ?>"><br>
    <label for="email">email : </label> <input type="mail" name="email" id="email" value="<?= $profil['MAIL_UTI'] ?>"><br>

    <label for="motdepasse">Mot de passe : </label> <input type="password" name="motdepasse" id="motdepasse"><br>
    <label>confirmer mot de passe : </label><input type="password" name="confirmermotdepasse" id="confirmermotdepasse"><br> <br> <br> <br>

    <input type="checkbox" name="hobbie[1]" id="1" value="1" ><label for="1">SPORT</label> <br>
    <input type="checkbox" name="hobbie[2]" id="2" value="2" ><label for="2">MUSIQUE</label> <br>
    <input type="checkbox" name="hobbie[3]" id="3" value="3" ><label for="3">CINEMA</label> <br>
    <input type="checkbox" name="hobbie[4]" id="4" value="4" ><label for="4">VOYAGE</label> <br>
    <input type="checkbox" name="hobbie[5]" id="5" value="5" ><label for="5">THEATRE</label> <br>
    <input type="checkbox" name="hobbie[6]" id="6" value="6" ><label for="6">VOITURE</label> <br>
    <input type="checkbox" name="hobbie[7]" id="7" value="7" ><label for="7">NOUVELLES TECHOLOGIES</label> <br>
    <input type="checkbox" name="hobbie[8]" id="8" value="8" ><label for="8">NOURRITURE</label> <br>
    <input type="checkbox" name="hobbie[9]" id="9" value="9" ><label for="9">MODE</label> <br>
    <input type="checkbox" name="hobbie[10]" id="10" value="10" ><label for="10">LITTERATURE</label> <br>
    <input type="checkbox" name="hobbie[11]" id="11" value="11" ><label for="11">ANIMAUX</label> <br>
    <input type="checkbox" name="hobbie[12]" id="12" value="12" ><label for="12">JEUX-VIDEOS</label> <br>
    <input type="checkbox" name="hobbie[13]" id="13" value="13" ><label for="13">SCIENCES</label> <br>
    <input type="checkbox" name="hobbie[14]" id="14" value="14" ><label for="14">SORTIR</label> <br>
    <input type="checkbox" name="hobbie[15]" id="15" value="15" ><label for="15">NETFLIX & CHILL</label> <br> <br> <br> <br>

<?php if($profil['ROL_UTI'] === '0'){ ?>

      <input type="radio" name="role" id="role_P" value="0" checked><label for="role_P">Passager</label>
      <input type="radio" name="role" id="role_C" value="1" ><label for="role_C">Conducteur</label> <br>

<?php }else{ ?>

    <input type="radio" name="role" id="role_P" value="0" ><label for="role_P">Passager</label>
    <input type="radio" name="role" id="role_C" value="1"  checked><label for="role_C">Conducteur</label> <br>

<?php } ?>

    <label for="marque">Marque : </label> <input type="text" name="marque" id="marque" value="<?= $profil['MRQ_VOIT_UTI'] ?>"><br>
    <label for="model">model : </label> <input type="text" name="model" id="model" value="<?= $profil['MDL_VOIT_UTI'] ?>"><br>
    <label for="place">Nombre de place : </label> <input type="number" name="place" id="place" value="<?= $profil['NB_PL_VOIT_UTI'] ?>"><br>

    <label for="assurance">Copie d'assurance :</label>
    <input type="file" name="assurance" id="assurance"> <br> <br>
    <input type="submit" value="Actualiser mon profil">
</form>

<?php
require_once './include/footer.php';
?>
