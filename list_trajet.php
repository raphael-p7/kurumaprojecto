<?php
$title = 'Rechercher un trajet';
require_once './include/header.php';

$result=$pdo->query('SELECT * FROM t_utilisateur WHERE ID_UTI = '.$_SESSION['ID_UTI']);
$profil = $result->fetch(PDO::FETCH_ASSOC);
$_SESSION['ID_UTI'] = $profil['ID_UTI'];
$_SESSION['VALID_UTI'] = $profil['VALID_UTI'];
$_SESSION['ADM_UTI'] = $profil['ADM_UTI'];


if (!isset($_SESSION['ID_UTI']) || empty($_SESSION['VALID_UTI'])) {
    header('Location: index.php');
}

  $req = $pdo->prepare('SELECT * FROM t_trajet WHERE DEPART_LATITUDE_TRA = :depart_latitude AND DEPART_LONGITUDE_TRA =:depart_longitude AND ARRIVER_LATITUDE_TRA =:arriver_latitude AND ARRIVER_LONGITUDE_TRA =:arriver_longitude');

    $req->execute([
      'depart_latitude' => $_POST['depart_latitude'],
      'depart_longitude' => $_POST['depart_longitude'],
      'arriver_latitude' => $_POST['arriver_latitude'],
      'arriver_longitude' => $_POST['arriver_longitude']
    ]);
    $res = $req->fetchAll(PDO::FETCH_ASSOC);
?>
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.3.1/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.3.1/mapbox-gl.css' rel='stylesheet' />
<style>
body { margin:0; padding:0; }
#map { position:absolute; top:0; bottom:0; width:50%; right: 0}
</style>

<div>
  <?php foreach ($res as $key) { ?>
    <tr>
        <td>
          <?php $result2=$pdo->query('SELECT NOM_UTI, PRENOM_UTI FROM t_utilisateur WHERE ID_UTI = '.$key['ID_UTI']);
            $id = $result2->fetch(PDO::FETCH_ASSOC);
            $nom = $id['NOM_UTI'];
            $prenom = $id['PRENOM_UTI'];
            echo ($nom);
            echo (" ");
            echo ( $prenom); ?>
        </td> <br>
        <td><?php echo $key['DEPART_NOM_TRA']; ?></td> <br>
        <td><?php echo $key['HEUR_DEPART_TRA']; ?></td> <br>
        <td><?php echo $key['ARRIVER_NOM_TRA']; ?></td> <br>
        <td><?php echo $key['PRIX_TRA']; ?> €</td> <br> <br>
    </tr>
<?php } ?>

</div>

<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.css' type='text/css' />
<div id='map'></div>

<script>
mapboxgl.accessToken = 'pk.eyJ1Ijoib21hbGxleTc2IiwiYSI6ImNrMG5oa29sYTFkc3ozbG5zMTg1aTZmZTgifQ.E87ATWOA3YeijDgrB2DRiA';

var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v10',
  center: ["<?php $_POST['depart_longitude']; ?>", "<?php $_POST['depart_latitude']; ?>"], // starting position
  zoom: 12
});
// initialize the map canvas to interact with later
var canvas = map.getCanvasContainer();


// map.addControl(new MapboxDirections({
// accessToken: mapboxgl.accessToken
//
// }), 'top-left');


// create a function to make a directions request
function getRoute() {
  // make a directions request using cycling profile
  // an arbitrary start will always be the same
  // only the end or destination will change
  var start = ["<?php $_POST['depart_longitude']; ?>", "<?php $_POST['depart_latitude']; ?>"];
  var end = ["<?php $_POST['arriver_longitude']; ?>", "<?php $_POST['arriver_latitude']; ?>"];

  console.log(start);
  console.log(end);

  var url = 'https://api.mapbox.com/directions/v5/mapbox/driving/' + start[0] + ',' + start[1] + ';' + end[0] + ',' + end[1] + '?steps=true&geometries=geojson&access_token=' + mapboxgl.accessToken;

  // make an XHR request https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
  var req = new XMLHttpRequest();
  req.responseType = 'json';
  req.open('GET', url, true);
  req.onload = function() {
    console.log("response", req.response)
    var data = req.response.routes[0];
    var route = data.geometry.coordinates;
    var geojson = {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'LineString',
        coordinates: route
      }
    };
    // if the route already exists on the map, reset it using setData
    if (map.getSource('route')) {
      map.getSource('route').setData(geojson);
    } else { // otherwise, make a new request
      map.addLayer({
        id: 'route',
        type: 'line',
        source: {
          type: 'geojson',
          data: geojson
        },
        layout: {
          'line-join': 'round',
          'line-cap': 'round'
        },
        paint: {
          'line-color': '#3887be',
          'line-width': 5,
          'line-opacity': 0.75
        }
      });
    }
  addPoint(start, '#3887be', 'start')
  addPoint(end, '#f30', 'end')
    // add turn instructions here at the end
  };

  map.setCenter(start);
  req.send();
}

map.on('load', function(){
  getRoute();
})

map.on('click', function(e) {

  getRoute();
});

// mettre une couleur est un point sur les lieu de depart et d'arrivé
function addPoint(position, color, name){
  if (map.getLayer(name)) {
 map.getSource(name).setData(position);
} else {
 map.addLayer({
   id: name,
   type: 'circle',
   source: {
     type: 'geojson',
     data: {
       type: 'FeatureCollection',
       features: [{
         type: 'Feature',
         properties: {},
         geometry: {
           type: 'Point',
           coordinates: position
         }
       }]
     }
   },
   paint: {
     'circle-radius': 10,
     'circle-color': color
   }
 });}
}

</script>


<?php
require_once './include/footer.php';
?>
