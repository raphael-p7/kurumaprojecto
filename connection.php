<?php
$title = 'Connexion';
require_once './include/header.php';

if (isset($_SESSION['ID_UTI'])) {
    header('Location: index.php');
}
if (!empty($_POST)){
    $req = $pdo->prepare('SELECT * FROM t_utilisateur WHERE MAIL_UTI = :MAIL_UTI');
    $req->execute([
        'MAIL_UTI' => $_POST['MAIL_UTI']
    ]);
    $res = $req->fetch(PDO::FETCH_ASSOC);
    $mdpCorrect = password_verify($_POST['MDP_UTI'], $res['MDP_UTI']);
    if (!$res || !$mdpCorrect) {
        echo 'Mauvais login ou mot de passe !';
    } else {
        $_SESSION['ID_UTI'] = $res['ID_UTI'];
        header('Location: index.php');
    }
}
?>

<form action="connection.php" method="POST">
    <label for="MAIL_UTI">mail : </label><input type="text" name="MAIL_UTI" id="MAIL_UTI"><br>
    <label for="MDP_UTI">Mot de passe : </label><input type="password" name="MDP_UTI" id="MDP_UTI"><br>
    <input type="submit" value="Se connecter">
</form>
<p><a href="inscription.php">Je n'ai pas encore de compte</a></p>

<?php
require_once './include/footer.php';
?>
