<?php
$title = 'Rechercher un trajet';
require_once './include/header.php';

    $result=$pdo->query('SELECT * FROM t_utilisateur WHERE ID_UTI = '.$_SESSION['ID_UTI']);
   $profil = $result->fetch(PDO::FETCH_ASSOC);
   $_SESSION['ID_UTI'] = $profil['ID_UTI'];
   $_SESSION['VALID_UTI'] = $profil['VALID_UTI'];
   $_SESSION['ADM_UTI'] = $profil['ADM_UTI'];

   if (!isset($_SESSION['ID_UTI']) || empty($_SESSION['VALID_UTI'])) {
       header('Location: index.php');
   }
?>
<form action="list_trajet.php" method="POST">
    <label for="depart_ville">Ville de départ : </label> <input autocomplete="off" type="text" name="depart_ville" id="depart_ville" onkeyup="foundLocation()"><br>
    <div id="list_depart">

    </div> <br>
    <input type="hidden" id="depart_latitude" name="depart_latitude" value="">
    <input type="hidden" id="depart_longitude" name="depart_longitude" value="">


    <label for="arriver_ville">Ville d'arrivé : </label> <input autocomplete="off" type="text" name="arriver_ville" id="arriver_ville" onkeyup="foundLocation2()"><br>
    <div id="list_arriver">

    </div> <br>
    <input type="hidden" id="arriver_latitude" name="arriver_latitude" value="">
    <input type="hidden" id="arriver_longitude" name="arriver_longitude" value="">

    <input type="submit" value="Rechercher un trajet">
</form>


<script>
  var inputDepart = document.getElementById("depart_ville");
  var inputArriver = document.getElementById("arriver_ville");
  var DIV1 = document.getElementById("list_depart");
  var DIV2 = document.getElementById("list_arriver");

function foundLocation(){
  if(inputDepart.value == ''){
    while(DIV1.firstChild){
      DIV1.removeChild(DIV1.firstChild);
    }
    return;
  }
  var URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+ inputDepart.value +".json?types=place,locality&access_token=pk.eyJ1Ijoib21hbGxleTc2IiwiYSI6ImNrMG5oa29sYTFkc3ozbG5zMTg1aTZmZTgifQ.E87ATWOA3YeijDgrB2DRiA";

  fetch(URL).then(function(response){
    return response.json();
  }).then(function(data){
    var results = data.features.map(function (el) {
      return {
        name: el.place_name,
        center: el.center
      }
    })
    while(DIV1.firstChild){
      DIV1.removeChild(DIV1.firstChild);
    }
    for(var i=0; i<results.length; i++){
      var div = document.createElement('div');
      div.innerHTML = results[i].name;
      div.setAttribute('data', results[i].name);
      div.setAttribute('longitude', results[i].center[0]);
      div.setAttribute('latitude', results[i].center[1]);

      div.addEventListener('click', function(e){

        inputDepart.value = e.target.getAttribute('data');
        document.getElementById('depart_longitude').value = e.target.getAttribute('longitude');
        document.getElementById('depart_latitude').value = e.target.getAttribute('latitude');
        while(DIV1.firstChild){
          DIV1.removeChild(DIV1.firstChild);
        }
      });

      DIV1.appendChild(div);
    }
  })
}

function foundLocation2(){
  if(inputArriver.value == ''){
    while(DIV2.firstChild){
      DIV2.removeChild(DIV2.firstChild);
    }
    return;
  }
  var URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+ inputArriver.value +".json?types=place,locality&access_token=pk.eyJ1Ijoib21hbGxleTc2IiwiYSI6ImNrMG5oa29sYTFkc3ozbG5zMTg1aTZmZTgifQ.E87ATWOA3YeijDgrB2DRiA";

  fetch(URL).then(function(response){
    return response.json();
  }).then(function(data){
    var results = data.features.map(function (el) {
      return {
        name: el.place_name,
        center: el.center
      }
    })
    while(DIV2.firstChild){
      DIV2.removeChild(DIV2.firstChild);
    }
    for(var i=0; i<results.length; i++){
      var div = document.createElement('div');
      div.innerHTML = results[i].name;
      div.setAttribute('data', results[i].name);
      div.setAttribute('longitude', results[i].center[0]);
      div.setAttribute('latitude', results[i].center[1]);

      div.addEventListener('click', function(e){

        inputArriver.value = e.target.getAttribute('data');
        document.getElementById('arriver_longitude').value = e.target.getAttribute('longitude');
        document.getElementById('arriver_latitude').value = e.target.getAttribute('latitude');

        while(DIV2.firstChild){
          DIV2.removeChild(DIV2.firstChild);
        }
      });
      DIV2.appendChild(div);
    }
  })
}

</script>


<?php
require_once './include/footer.php';
?>
