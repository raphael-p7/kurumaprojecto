<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
<title>Map</title>
<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.3.1/mapbox-gl.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.3.1/mapbox-gl.css' rel='stylesheet' />
<style>
body { margin:0; padding:0; }
#map { position:absolute; top:0; bottom:0; width:100%; }
</style>
</head>
<body>

<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.js'></script>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.css' type='text/css' />
<div id='map'></div>

<script>
mapboxgl.accessToken = 'pk.eyJ1Ijoib21hbGxleTc2IiwiYSI6ImNrMG5oa29sYTFkc3ozbG5zMTg1aTZmZTgifQ.E87ATWOA3YeijDgrB2DRiA';

var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v10',
  center: [-122.662323, 45.523751], // starting position
  zoom: 12
});
// set the bounds of the map
var bounds = [[-123.069003, 45.395273], [-122.303707, 45.612333]];
map.setMaxBounds(bounds);

// initialize the map canvas to interact with later
var canvas = map.getCanvasContainer();

// an arbitrary start will always be the same
// only the end or destination will change
var start = [-122.662323, 45.523751];

map.addControl(new MapboxDirections({
accessToken: mapboxgl.accessToken

}), 'top-left');


// create a function to make a directions request
function getRoute() {
  // make a directions request using cycling profile
  // an arbitrary start will always be the same
  // only the end or destination will change
  var start = [-122.662323, 45.523751];
  var end = [  -122.64000702099588, 45.52290911570722 ];
  var url = 'https://api.mapbox.com/directions/v5/mapbox/cycling/' + start[0] + ',' + start[1] + ';' + end[0] + ',' + end[1] + '?steps=true&geometries=geojson&access_token=' + mapboxgl.accessToken;

  // make an XHR request https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
  var req = new XMLHttpRequest();
  req.responseType = 'json';
  req.open('GET', url, true);
  req.onload = function() {
    console.log("response", req.response)
    var data = req.response.routes[0];
    var route = data.geometry.coordinates;
    var geojson = {
      type: 'Feature',
      properties: {},
      geometry: {
        type: 'LineString',
        coordinates: route
      }
    };
    // if the route already exists on the map, reset it using setData
    if (map.getSource('route')) {
      map.getSource('route').setData(geojson);
    } else { // otherwise, make a new request
      map.addLayer({
        id: 'route',
        type: 'line',
        source: {
          type: 'geojson',
          data: geojson
        },
        layout: {
          'line-join': 'round',
          'line-cap': 'round'
        },
        paint: {
          'line-color': '#3887be',
          'line-width': 5,
          'line-opacity': 0.75
        }
      });
    }
  addPoint(start, '#3887be', 'start')
  addPoint(end, '#f30', 'end')
    // add turn instructions here at the end
  };

  map.setCenter(start);
  req.send();
}

map.on('load', function(){
  getRoute();
})

map.on('click', function(e) {

  getRoute();
});

// mettre une couleur est un point sur les lieu de depart et d'arrivé
function addPoint(position, color, name){
  if (map.getLayer(name)) {
 map.getSource(name).setData(position);
} else {
 map.addLayer({
   id: name,
   type: 'circle',
   source: {
     type: 'geojson',
     data: {
       type: 'FeatureCollection',
       features: [{
         type: 'Feature',
         properties: {},
         geometry: {
           type: 'Point',
           coordinates: position
         }
       }]
     }
   },
   paint: {
     'circle-radius': 10,
     'circle-color': color
   }
 });}
}

</script>

</body>
</html>
