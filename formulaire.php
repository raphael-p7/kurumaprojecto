<?php

if(empty($_POST['nom'])          ||
  empty($_POST['email'])         ||
  empty($_POST['objet'])       ||
  empty($_POST['message'])    ||
  !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
  {
    echo "Argument(s) manquant(s)";
    return false;
  }

$name = $_POST['nom'];
$email_address = $_POST['email'];
$objet = $_POST['objet'];
$message = $_POST['message'];


$to = 'valentin.lapointe@viacesi.fr';
$email_subject = "vous avez un message de :  $name";
$email_body = "Message reçu.\n\n"."Details:\n\nName: $name\n\nObjet: $objet\n\nEmail: $email_address\n\nMessage:\n$message";
$headers = "De: noreply@yourdomain.com\n";
$headers .= "Reply-To: $email_address";
mail($to,$email_subject,$email_body,$headers);

header('location:CV.html');
return true;
?>
