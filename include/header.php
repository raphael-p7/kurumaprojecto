<?php

require_once 'BDD.php';
session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?></title>

</head>

<body>

<nav class="navbar is-link" id="main_menu">
    <div class="navbar-start">
        <h1 class="navbar-item navbar-brand title"><a href="index.php">Kuruma</a></h1>
    </div>

        <ul>
          <li><a href="creation_trajet.php">Proposer un trajet</a></li>
          <li><a href="recherche_trajet.php">Rechercher</a></li>
          <li><a href="profil.php">Profil</a></li>

          <!--Affichage uniquement si un admin est connecter-->
          <?php  echo (isset($_SESSION['ADM_UTI'])) ? '<li><a class="has-text-white"  href="admin.php">Page Admin</a></li>' : '' ?>

          <?php  if(isset($_SESSION['ID_UTI'])) {?>
          <li><a href="deconnection.php">Déconnexion</a></li>
          <?php  } else {?>
            <li><a href="connection.php">Connexion</a></li>
          <?php } ?>
        </ul>
</nav>
