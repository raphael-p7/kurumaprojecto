<?php
$title = 'Inscription';
require_once './include/header.php';

if (isset($_SESSION['ID_UTI'])) {
    header('Location: index.php');
}

if (!empty($_POST)){

try{
  $try = $pdo->prepare('SELECT MAIL_UTI FROM t_utilisateur WHERE MAIL_UTI = :mail');
  $try->execute([
      'mail' => $_POST['email']
  ]);
  $res = $try->fetch(PDO::FETCH_ASSOC);
}catch(PDOException $e){
  echo 'Échec lors de la connexion : ' . $e->getMessage();
}

  if($res['MAIL_UTI'] === $_POST['email']){
    echo "Erreur, l'adresse mail est déjà entrée dans la BDD";
  }else{

  $age = strlen($_POST['age']);

  $nom = explode(" ", $_POST['nom']);
  $nom = implode("", $nom);

  $prenom = explode(" ", $_POST['prenom']);
  $prenom = implode("", $prenom);

  $marque = explode(" ", $_POST['marque']);
  $marque = implode("", $marque);

  $model = explode(" ", $_POST['model']);
  $model = implode("", $model);

  $mdp1 = $_POST['motdepasse'];
  $mdp2 = $_POST['confirmermotdepasse'];

  $place = strlen($_POST['place']);

  if (!ctype_alpha($_POST['prenom'])) {
      echo "Erreur, un prénom ne peut avoir de chiffre";
  } else {

    if ($age > 4) {
    echo "Erreur, age de mauvaise taille";
    } else {

      if ($place > 2) {
      echo "Erreur, le nombre de place de votre vehicule est trop grand";
      } else {

        if((empty($marque) || empty($model) || empty($_POST['place'])) && ($_POST['role'] === '1')){
          echo "Erreur, les details du vehicule sont incomplets";
        }else{

        if(empty($_POST['assurance']) && ($_POST['role'] === '1')){
          echo "Erreur, l'assurance est OBLIGATOIRE !!!'";
        }else{



          if( $_POST['role'] === '0'){
            $valide = '1';
          }else{
            $valide = '0';
          }

        if ($mdp1 == $mdp2) {

          if(empty($_POST['place'])){
            $_POST['place'] = '0';
          }

    $req = $pdo->prepare('INSERT INTO t_utilisateur (MAIL_UTI, NOM_UTI, PRENOM_UTI, AGE_UTI, MDP_UTI, ROL_UTI, ADM_UTI, MRQ_VOIT_UTI, MDL_VOIT_UTI, NB_PL_VOIT_UTI, ASSU_VOIT_UTI, VALID_UTI)
                                      VALUES (:email, :nom, :prenom, :age, :motdepasse, :role, :admin, :marque, :model, :place, :assurance, :valide)');
    $req->execute([
        'nom' => $nom,
        'prenom' => $prenom,
        'email' => $_POST['email'],
        'age' => $_POST['age'],
        'motdepasse' => password_hash($_POST['motdepasse'], PASSWORD_DEFAULT),
        'role' => $_POST['role'],
        'marque' => $marque,
        'model' => $model,
        'place' => $_POST['place'],
        'assurance' => $_POST['assurance'],
        'valide' => $valide,
        'admin' => '0',
    ]);

    $arrayHobbie = $_POST['hobbie'];

    foreach($_POST['hobbie'] as $valeur)
{
   $req2 = $pdo->prepare('INSERT INTO aimer (ID_UTI, ID_HOB)
                                     VALUES (LAST_INSERT_ID(), :id_hobbies)');
   $req2->execute([
     'id_hobbies' => $valeur,
   ]);
};

    header('Location: connection.php');
  }else{
    echo "mot de passe non conforme";
  }
}
}
}
}
}
}
}
?>
<form action="inscription.php" method="POST">
    <label for="prenom">Nom : </label> <input type="text" name="nom" id="nom"><br>
    <label for="prenom">Prenom : </label> <input type="text" name="prenom" id="prenom"><br>
    <label for="age">Age : </label> <input type="number" name="age" id="age"><br>
    <label for="email">email : </label> <input type="mail" name="email" id="email"><br>

    <label for="motdepasse">Mot de passe : </label> <input type="password" name="motdepasse" id="motdepasse"><br>
    <label>confirmer mot de passe : </label><input type="password" name="confirmermotdepasse" id="confirmermotdepasse"><br> <br> <br> <br>

    <input type="checkbox" name="hobbie[1]" id="1" value="1" ><label for="1">SPORT</label> <br>
    <input type="checkbox" name="hobbie[2]" id="2" value="2" ><label for="2">MUSIQUE</label> <br>
    <input type="checkbox" name="hobbie[3]" id="3" value="3" ><label for="3">CINEMA</label> <br>
    <input type="checkbox" name="hobbie[4]" id="4" value="4" ><label for="4">VOYAGE</label> <br>
    <input type="checkbox" name="hobbie[5]" id="5" value="5" ><label for="5">THEATRE</label> <br>
    <input type="checkbox" name="hobbie[6]" id="6" value="6" ><label for="6">VOITURE</label> <br>
    <input type="checkbox" name="hobbie[7]" id="7" value="7" ><label for="7">NOUVELLES TECHOLOGIES</label> <br>
    <input type="checkbox" name="hobbie[8]" id="8" value="8" ><label for="8">NOURRITURE</label> <br>
    <input type="checkbox" name="hobbie[9]" id="9" value="9" ><label for="9">MODE</label> <br>
    <input type="checkbox" name="hobbie[10]" id="10" value="10" ><label for="10">LITTERATURE</label> <br>
    <input type="checkbox" name="hobbie[11]" id="11" value="11" ><label for="11">ANIMAUX</label> <br>
    <input type="checkbox" name="hobbie[12]" id="12" value="12" ><label for="12">JEUX-VIDEOS</label> <br>
    <input type="checkbox" name="hobbie[13]" id="13" value="13" ><label for="13">SCIENCES</label> <br>
    <input type="checkbox" name="hobbie[14]" id="14" value="14" ><label for="14">SORTIR</label> <br>
    <input type="checkbox" name="hobbie[15]" id="15" value="15" ><label for="15">NETFLIX & CHILL</label> <br> <br> <br> <br>

    <input type="radio" name="role" id="role_P" value="0" ><label for="role_P">Passager</label>
    <input type="radio" name="role" id="role_C" value="1" ><label for="role_C">Conducteur</label> <br>

    <label for="marque">Marque : </label> <input type="text" name="marque" id="marque"><br>
    <label for="model">model : </label> <input type="text" name="model" id="model"><br>
    <label for="place">Nombre de place : </label> <input type="number" name="place" id="place"><br>

    <label for="assurance">Copie d'assurance :</label>
    <input type="file" name="assurance" id="assurance"> <br> <br>
    <input type="submit" value="S'inscrire">
</form>
<p><a href="connection.php">J'ai déjà un compte</a></p>

<?php
require_once './include/footer.php';
?>
