<?php
$title = 'Admin';
require_once './include/header.php';

if (!isset($_SESSION['ID_UTI']) || empty($_SESSION['VALID_UTI']) || !isset($_SESSION['ADM_UTI'])) {
    header('Location: index.php');
}


if (!empty($_POST)){
  $req = $pdo->prepare('UPDATE t_utilisateur SET VALID_UTI = :valide WHERE ID_UTI = '.$_POST['ID_UTI']);
  $req->execute([
      'valide' => '1',
  ]);

} else {
$result=$pdo->query('SELECT * FROM t_utilisateur WHERE ID_UTI = '.$_SESSION['ID_UTI']);
$profil = $result->fetch(PDO::FETCH_ASSOC);
$_SESSION['ID_UTI'] = $profil['ID_UTI'];
$_SESSION['VALID_UTI'] = $profil['VALID_UTI'];
$_SESSION['ADM_UTI'] = $profil['ADM_UTI'];
}

$req = $pdo->prepare('SELECT * FROM t_utilisateur WHERE VALID_UTI =:valid_uti AND ROL_UTI =:role_uti');

  $req->execute([
    'valid_uti' => '0',
    'role_uti' => '1',
  ]);
  $res = $req->fetchAll(PDO::FETCH_ASSOC);

?>

<div>
  <?php foreach ($res as $key) { ?>
  <form action="admin.php" method="POST">
    <tr>
        <td><?php echo $key['NOM_UTI']; ?></td> <br>
        <td><?php echo $key['PRENOM_UTI']; ?></td> <br>
        <td><?php echo $key['AGE_UTI']; ?></td> <br>
        <td><?php echo $key['MRQ_VOIT_UTI']; ?></td> <br>
        <td><?php echo $key['MDL_VOIT_UTI']; ?></td> <br>
        <td><?php echo $key['NB_PL_VOIT_UTI']; ?></td> <br>
        <img src="<?php echo $key['ASSU_VOIT_UTI']; ?>" alt=""> <br>
        <input type="hidden" name="ID_UTI" value="<?= $key['ID_UTI']?>">

        <input type="submit" value="Valider le compte">
    </tr>
  </form>
  <?php } ?>

</div>

<?php
require_once './include/footer.php';
?>
