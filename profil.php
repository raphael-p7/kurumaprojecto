<?php
$title = 'Profil';
require_once './include/header.php';

if (!isset($_SESSION['ID_UTI']) || empty($_SESSION['VALID_UTI'])) {
    header('Location: index.php');
}
if (!empty($_POST)) {
$result = $pdo->prepare('UPDATE t_utilisateur SET MAIL_UTI=:MAIL_UTI, MDP_UTI=:MDP_UTI, NOM_UTI=:NOM_UTI  WHERE ID_UTI = :ID_UTI');

$result->execute([
  'ID_UTI'=>$_POST['ID_UTI'],
  'MAIL_UTI'=>$_POST['MAIL_UTI'],
  'MDP_UTI'=>$_POST['MDP_UTI'],
  'NOM_UTI'=>$_POST['NOM_UTI']
]);
header('location: index.php');


}else{
  $result=$pdo->query('SELECT * FROM t_utilisateur WHERE ID_UTI = '.$_SESSION['ID_UTI']);
 $profil = $result->fetch(PDO::FETCH_ASSOC);
 $_SESSION['ID_UTI'] = $profil['ID_UTI'];
 $_SESSION['VALID_UTI'] = $profil['VALID_UTI'];
 $_SESSION['ADM_UTI'] = $profil['ADM_UTI'];
}


if ($profil["ROL_UTI"] === '0') {
  $role = 'Passager';
}else{
  $role = 'Conducteur';
}

$result2=$pdo->query('SELECT * FROM aimer AS a
                      INNER JOIN t_hobbies AS h ON a.ID_HOB = h.ID_HOB
                      WHERE ID_UTI = '.$_SESSION['ID_UTI']);
$aimer = $result2->fetchAll(PDO::FETCH_ASSOC);


?>

<div>
    <p>AVATAR : <?= $profil["AVATAR_UTI"] ?></p>
    <p>Nom : <?= $profil["NOM_UTI"] ?></p>
    <p>Prenom : <?= $profil["PRENOM_UTI"] ?></p>
    <p>Mail : <?= $profil["MAIL_UTI"] ?></p>
    <p>Age : <?= $profil["AGE_UTI"] ?></p>
    <p>Role : <?= $role ?></p>
    <p>Hobbies :</p> <br>
    <?php foreach ($aimer as $key){ ?>
      <p>- <?= $key['NOM_HOB']; ?></p>
    <?php } ?> <br>

    <a href="modif_profil.php">Modifier votre profil</a>
</div>


<?php
require_once './include/footer.php';
?>
